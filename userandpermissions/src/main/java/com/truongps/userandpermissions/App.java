package com.truongps.userandpermissions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class App {

	public class InvalidNumberOfUsersException extends Exception {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public InvalidNumberOfUsersException() {
			super("The number of users must be in (0, 100000).");
		}
	}

	public class InvalidNumberOfPermissionsException extends Exception {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public InvalidNumberOfPermissionsException() {
			super("The number of Permissions must be in (0, 100).");
		}
	}

	public static void main(String[] args)
			throws IOException, InvalidNumberOfUsersException, InvalidNumberOfPermissionsException {
		App app = new App();

		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String numberOfUsersLine = reader.readLine();

		boolean isOnlyDigits = numberOfUsersLine.matches("\\d+");
		if (isOnlyDigits) {
			int numberOfUsers = Integer.parseInt(numberOfUsersLine);
			app.setNumberOfUsers(numberOfUsers);

			String ceoPermissions = reader.readLine();
			app.setUserWithPermissions(ceoPermissions);

			String userPermissions = null;
			for (int i = 0; i < numberOfUsers; i++) {
				userPermissions = reader.readLine();
				app.setUserWithPermissions(userPermissions);
			}

			String manager = null;
			for (int i = 0; i < numberOfUsers; i++) {
				manager = reader.readLine();
				app.setManager(manager);
			}

			List<String> allUserPermissions = app.queryAllUserPermissions();
			for (int i = 0; i < allUserPermissions.size(); i++) {
				System.out.println(allUserPermissions.get(i));
			}

			String moreOpt = null;
			String returnedValue = null;
			do {
				moreOpt = reader.readLine();
				if (moreOpt.length() > 0) {
					returnedValue = app.doOp(moreOpt);
					if (returnedValue != null && returnedValue.length() > 0) {
						System.out.println(returnedValue);
					}
				}
			} while (moreOpt.length() > 0);

		} else {
			System.out.println("Please restart the program!!!");
		}
	}

	public static final int MIN_USER = 0;
	public static final int MAX_USER = 100000;

	public static final String OP_ADD = "ADD";
	public static final String OP_QUERY = "QUERY";
	public static final String OP_REMOVE = "REMOVE";
	public static final String EMPTY_STRING = "";
	public static final String CEO = "CEO";

	private int numberOfUsers;
	private Map<String, Set<String>> userPermissions = new LinkedHashMap<String, Set<String>>();
	private Map<String, Set<String>> userManagers = new HashMap<String, Set<String>>();
	private int addManagerCount = 0;

	public int getNumberOfUsers() {
		return numberOfUsers;
	}

	/**
	 * Time Complexity: O(1). It takes O(1) time.
	 * 
	 * Space Complexity: O(1).
	 * 
	 * @param numberOfUsers
	 * @throws InvalidNumberOfUsersException
	 */
	public void setNumberOfUsers(int numberOfUsers) throws InvalidNumberOfUsersException {
		if (numberOfUsers <= MIN_USER || numberOfUsers >= MAX_USER) {
			throw new InvalidNumberOfUsersException();
		}
		this.numberOfUsers = numberOfUsers;
	}

	/**
	 * Time Complexity: O(1).
	 * 
	 * Space Complexity: O(1).
	 * 
	 * @param permissionString
	 * @return
	 * @throws InvalidNumberOfPermissionsException
	 */
	public String setUserWithPermissions(String permissionString) throws InvalidNumberOfPermissionsException {
		if (permissionString == null || permissionString.isEmpty()) {
			throw new InvalidNumberOfPermissionsException();
		} else {
			String[] permissions = permissionString.split(" ");
			if (permissions.length < 1 || permissions.length > 99) {
				throw new InvalidNumberOfPermissionsException();
			} else {
				int userCount = userPermissions.size();
				String user = null;
				if (userCount == 0) {
					user = CEO;
				} else {
					user = String.valueOf(userCount);
				}
				Set<String> permissionList = convertArrayToSet(permissions);
				userPermissions.put(user, permissionList);
				return user;
			}
		}
	}

	/**
	 * Convert an Array of strings to a Set of strings.
	 * 
	 * Time Complexity: O(n).
	 * 
	 * Space Complexity: O(1).
	 * 
	 * @param array
	 * @return
	 */
	private Set<String> convertArrayToSet(String array[]) {
		Set<String> set = new HashSet<String>();
		for (int i = 0; i < array.length; i++) {
			set.add(array[i]);
		}
		return set;
	}

	/**
	 * Set a manager to a user.
	 * 
	 * Time Complexity: O(1).
	 * 
	 * Space Complexity: O(1).
	 * 
	 * @param manager
	 */
	public void setManager(String manager) {
		if (addManagerCount < this.numberOfUsers) {
			addManagerCount++;
			String user = String.valueOf(addManagerCount);
			Set<String> members = null;
			if (userManagers.containsKey(manager)) {
				members = userManagers.get(manager);
			} else {
				members = new HashSet<String>();
				userManagers.put(manager, members);
			}
			members.add(user);
		} else {
			throw new RuntimeException("The number of manager input is large or equal the number of users.");
		}
	}

	/**
	 * Get all permissions of a user.
	 * 
	 * Time Complexity: O(n). Runs in linear O(n) time. In the worst case scenario,
	 * We have to iterate the entire the users to find all permissions.
	 * 
	 * Space Complexity: O(1).
	 * 
	 * @param user
	 * @param collectedPermissions
	 */
	private void getUserPermissions(String user, Set<String> collectedPermissions) {
		collectedPermissions.addAll(userPermissions.get(user));
		if (userManagers.containsKey(user)) {
			Set<String> members = userManagers.get(user);
			for (String member : members) {
				getUserPermissions(member, collectedPermissions);
			}
		}
	}

	/**
	 * Convert a set of strings to a String.
	 * 
	 * Time Complexity: O(n).
	 * 
	 * Space Complexity: O(n). StringBuilder makes use of a resizable array. When a
	 * new String is appended, its characters are copied to the end of the array.
	 * 
	 * @param permissions
	 * @return
	 */
	private String permissionToString(Set<String> permissions) {
		final String COMMA = ", ";

		StringBuilder stringBuilder = new StringBuilder();
		for (String permission : permissions) {
			if (stringBuilder.length() > 0) {
				stringBuilder.append(COMMA);
			}
			stringBuilder.append(permission);
		}
		return stringBuilder.toString();
	}

	/**
	 * Time Complexity: O(n2). In the worst case scenario, We have to iterate the
	 * entire the users to find all permissions for all users.
	 * 
	 * Space Complexity: O(n). Because of permissionToString function.
	 * 
	 * @return
	 */
	public List<String> queryAllUserPermissions() {
		List<String> all = new ArrayList<String>();

		Set<String> permissions = null;
		String permissionString = null;
		for (String user : userPermissions.keySet()) {
			permissions = new HashSet<String>();
			getUserPermissions(user, permissions);
			permissionString = permissionToString(permissions);
			all.add(permissionString);
		}
		return all;
	}

	/**
	 * Support 3 functions: ADD, QUERY, and REMOVE.
	 * 
	 * @param inputFormat
	 * @return
	 */
	public String doOp(String inputFormat) {
		String[] splitedString = inputFormat.split(" ");

		String opt = splitedString[0];
		String user = splitedString[1];

		if (OP_ADD.equals(opt)) {
			String permission = splitedString[2];
			addPermission(user, permission);
			return null;
		} else if (OP_QUERY.equals(opt)) {
			return queryUserPermissions(user);
		} else if (OP_REMOVE.equals(opt)) {
			String permission = splitedString[2];
			removePermission(user, permission);
			return null;
		} else {
			return null;
		}
	}

	/**
	 * Add one more permission to an user.
	 * 
	 * Time Complexity: O(1).
	 * 
	 * Space Complexity: O(1).
	 * 
	 * @param user
	 * @param permission
	 */
	private void addPermission(String user, String permission) {
		if (userPermissions.containsKey(user)) {
			userPermissions.get(user).add(permission);
		}
	}

	/**
	 * Query all permissions of an user.
	 * 
	 * Time Complexity: O(n). Runs in linear O(n) time. In the worst case scenario,
	 * We have to iterate the entire the users to find all permissions.
	 * 
	 * Space Complexity: O(n). Because of permissionToString function.
	 * 
	 * @param user
	 * @return
	 */
	private String queryUserPermissions(String user) {
		if (userPermissions.containsKey(user)) {
			Set<String> permissions = new HashSet<String>();
			getUserPermissions(user, permissions);
			return permissionToString(permissions);
		} else {
			return EMPTY_STRING;
		}
	}

	/**
	 * Remove one permission from an user.
	 * 
	 * Time Complexity: O(1).
	 * 
	 * Space Complexity: O(1).
	 * 
	 * @param user
	 * @param permission
	 */
	private void removePermission(String user, String permission) {
		if (userPermissions.containsKey(user)) {
			userPermissions.get(user).remove(permission);
		}
	}
}
