package com.truongps.userandpermissions;

import java.util.ArrayList;
import java.util.List;

import com.truongps.userandpermissions.App.InvalidNumberOfPermissionsException;
import com.truongps.userandpermissions.App.InvalidNumberOfUsersException;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class AppTest extends TestCase {
	/**
	 * Create the test case
	 *
	 * @param testName name of the test case
	 */
	public AppTest(String testName) {
		super(testName);
	}

	/**
	 * @return the suite of tests being tested
	 */
	public static Test suite() {
		return new TestSuite(AppTest.class);
	}

	public void testApp() throws InvalidNumberOfUsersException, InvalidNumberOfPermissionsException {
		List<String> expectedResults = new ArrayList<String>();
		expectedResults.add("A, B, C, D, E, F");
		expectedResults.add("A, B, C, D");
		expectedResults.add("A, B, C, E");
		expectedResults.add("A");
		expectedResults.add("D");
		expectedResults.add("A, C");
		expectedResults.add("A, B");

		App app = new App();
		app.setNumberOfUsers(6);

		app.setUserWithPermissions("A F");
		app.setUserWithPermissions("A B");
		app.setUserWithPermissions("A C E");
		app.setUserWithPermissions("A");
		app.setUserWithPermissions("D");
		app.setUserWithPermissions("A C");
		app.setUserWithPermissions("A B");

		app.setManager("CEO");
		app.setManager("CEO");
		app.setManager("1");
		app.setManager("1");
		app.setManager("1");
		app.setManager("2");

		List<String> allUserPermissions = app.queryAllUserPermissions();

		assertTrue(allUserPermissions.size() == expectedResults.size());

		for (int i = 0; i < allUserPermissions.size(); i++) {
			assertTrue(allUserPermissions.get(i).equals(expectedResults.get(i)));
			System.out.println(allUserPermissions.get(i));
		}

		app.doOp("ADD 2 X");

		String query2 = app.doOp("QUERY 2");
		assertTrue("A, B, C, E, X".equals(query2));
		System.out.println(query2);

		String queryCEO = app.doOp("QUERY CEO");
		assertTrue("A, B, C, D, E, F, X".equals(queryCEO));
		System.out.println(queryCEO);

		app.doOp("REMOVE 2 X");

		query2 = app.doOp("QUERY 2");
		assertTrue("A, B, C, E".equals(query2));
		System.out.println(query2);

		queryCEO = app.doOp("QUERY CEO");
		assertTrue("A, B, C, D, E, F".equals(queryCEO));
		System.out.println(queryCEO);
	}
}
